/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author a
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human H1 = new Human("JM");
        H1.eat();
        H1.walk();
        H1.run();
        H1.speak();
        H1.sleep();
        System.out.println(H1);
        System.out.println("_______________________________________");

        Cat C1 = new Cat("Pao");
        C1.eat();
        C1.walk();
        C1.run();
        C1.speak();
        C1.sleep();
        System.out.println(C1);
        System.out.println("_______________________________________");

        Crocodile Cr1 = new Crocodile("JorDan");
        Cr1.eat();
        Cr1.crawl();
        Cr1.walk();
        Cr1.speak();
        Cr1.sleep();
        System.out.println(Cr1);
        System.out.println("_______________________________________");

        Dog D1 = new Dog("Thongdum");
        D1.eat();
        D1.walk();
        D1.run();
        D1.speak();
        D1.sleep();
        System.out.println(D1);
        System.out.println("_______________________________________");

        Snake S1 = new Snake("Nagini");
        S1.eat();
        S1.crawl();
        S1.walk();
        S1.speak();
        S1.sleep();
        System.out.println(S1);
        System.out.println("_______________________________________");

        Crab Cb1 = new Crab("PuPe");
        Cb1.eat();
        Cb1.swim();
        Cb1.walk();
        Cb1.speak();
        Cb1.sleep();
        System.out.println(Cb1);
        System.out.println("_______________________________________");

        Fish F1 = new Fish("Neno");
        F1.eat();
        F1.swim();
        F1.walk();
        F1.speak();
        F1.sleep();
        System.out.println(F1);
        System.out.println("_______________________________________");

        Bat B1 = new Bat("Batman");
        B1.eat();
        B1.fly();
        B1.walk();
        B1.speak();
        B1.sleep();
        System.out.println(B1);
        System.out.println("_______________________________________");

        Bird Bd1 = new Bird("ThongChai");
        Bd1.eat();
        Bd1.fly();
        Bd1.walk();
        Bd1.speak();
        Bd1.sleep();
        System.out.println(Bd1);
        System.out.println("_______________________________________");

        System.out.println("------------------------------ Polymorphism Test -------------------------------- ");
        Animal[] animals = {H1, C1, Bd1, B1, F1, Cb1, S1, Cr1, D1};
        for (int i = 0; i < animals.length; i++) {
            if (animals[i] instanceof Animal) {
                animals[i].eat();
                animals[i].walk();
                animals[i].speak();
                animals[i].sleep();
            }
            if (animals[i] instanceof LandAnimal) {
                System.out.println("Im in LandAnimal Class ");
                ((LandAnimal) (animals[i])).run();

            }
            if (animals[i] instanceof Reptile) {
                System.out.println("Im in Reptile Class ");
                ((Reptile) (animals[i])).crawl();

            }
            if (animals[i] instanceof Poultry) {
                System.out.println("Im in Poultry Class ");
                ((Poultry) (animals[i])).fly();

            }
            if (animals[i] instanceof AquaticAnimal) {
                System.out.println("Im in Aquatic Class ");
                ((AquaticAnimal) (animals[i])).swim();

            }
        }

        System.out.println("------------------------------- Polymorphism Test 2 -------------------------------- ");
        for (int i = 0; i < animals.length; i++) {
            System.out.println(animals[i].getName() + " Instanceof Animal " + (animals[i] instanceof Animal));
            System.out.println(animals[i].getName() + " Instanceof Aquatic " + (animals[i] instanceof AquaticAnimal));
            System.out.println(animals[i].getName() + " Instanceof LandAnimal " + (animals[i] instanceof LandAnimal));
            System.out.println(animals[i].getName() + " Instanceof Poultry " + (animals[i] instanceof Poultry));
            System.out.println(animals[i].getName() + " Instanceof Reptile " + (animals[i] instanceof Reptile));
            System.out.println(animals[i].getName() + " Instanceof Object " + (animals[i] instanceof Object));
        }

    }
}
