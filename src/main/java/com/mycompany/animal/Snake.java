/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author a
 */
public class Snake extends Reptile {

    private String nickname;

    public Snake(String nickname) {
        super("Snake", 0);
        this.nickname = nickname;
    }

    /**
     *
     */
    @Override
    public void crawl() {
        System.out.println("Snake name is : " + nickname + " Can crawl ");
    }

    @Override
    public void eat() {
        System.out.println("Snake name is : " + nickname + " Can eat ");
    }

    @Override
    public void walk() {
        System.out.println("Snake name is : " + nickname + " Can't walk ");
    }

    @Override
    public void speak() {
        System.out.println("Snake name is : " + nickname + " speak >> foor foor !!");
    }

    @Override
    public void sleep() {
        System.out.println("Snake name is : " + nickname + " Can sleep ");
    }

}
