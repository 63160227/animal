/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author a
 */
public class Crab extends AquaticAnimal {
    private String name;

    public Crab (String name) { 
        super("crab", 8);
        this.name = name;
    }

    @Override
    public void swim() {
        System.out.println("crab name is : " + name + " Can swim");
    }

    @Override
    public void eat() {
       System.out.println("crab name is : " + name + " Can eat");
    }
    
    @Override
    public void walk() {
       System.out.println("crab name is : " + name +  " walk with " + numberOfLeg  + " legs." );
    }

    @Override
    public void speak() {
        System.out.println("crab name is : " + name + " Can speak");
    }

    /**
     *
     */
    @Override
    public void sleep() {
        System.out.println("Crab name is : " + name + " Can sleep");
    }
    
}

