/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author a
 */
public class Fish extends AquaticAnimal{
    private String name;

    public Fish(String name) {
        super("Fish", 0);
        this.name = name;
    }

    @Override
    public void swim() {
        System.out.println("Fish name is : " + name + " Can swim ");
    }

    @Override
    public void eat() {
        System.out.println("Fish name is : " + name + " Can eat");
    }

    @Override
    public void walk() {
        System.out.println("Fish name is : " + name + " Can't walk ");
    }

    @Override
    public void speak() {
        System.out.println("Fish name is : " + name + " Can't speak ");
    }

    /**
     *
     */
    @Override
    public void sleep() {
        System.out.println("Fish name is : " + name + " Can sleep ");
    }
    
}

    
