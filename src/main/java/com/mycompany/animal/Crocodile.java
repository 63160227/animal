/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author a
 */
public class Crocodile extends Reptile {

    private String name;

    public Crocodile(String name) {
        super("Crocodile", 4);
        this.name = name;
    }

    /**
     *
     */
    @Override
    public void crawl() {
        System.out.println("Crocodile name is : " + name + " Can crawl ");
    }

    @Override
    public void eat() {
        System.out.println("Crocodile name is : " + name + " Can eat ");
    }

    @Override
    public void walk() {
        System.out.println("Crocodile name is : " + name + " walk with " + numberOfLeg + " legs.");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile name is : " + name + " Can't speak ");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile name: " + name + " Can sleep ");
    }

}
